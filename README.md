# Project Utilities: Help Files and Scripts

Welcome to the Project Utilities repository! This collection of files and scripts has been curated to streamline and enhance various aspects of my workflow. The organization ensures easy access to the resources essential for efficient project management.

Feel free to explore, adapt, and optimize these utilities to match your project requirements and amplify your productivity. 

## Minicom

The Minicom script simplifies the configuration and execution of the minicom terminal communication program. It facilitates effortless interaction with serial devices, records communication in designated log files, and optimizes baud rates for smooth operations.

### Usage

To utilize the script, provide the path where you wish to save the log file. In case no path is specified, the script defaults to saving logs in the /tmp directory.

```bash
minicom.sh PATH_TO_LOG_FILE
```

## VSCode CMake Build and SDK-Integrated Debugging

Explore the vscode_sdk directory to uncover a comprehensive set of files essential for seamlessly building and debugging with the SDK. These scripts and resources are designed to simplify the creation of binaries using the SDK and to streamline debugging via gdb server. To harness their power, integrate these materials into your project's .vscode folder.

An essential step involves adapting the settings.json file to align with your SDK paths and tailor other settings to your needs.

### Build Process

Initiate the build process effortlessly by using the shortcut `Ctrl + Shift + B` within Visual Studio Code. This action prompts the compilation target selection.

