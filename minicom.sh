#! /bin/sh

LOG_PATH=$1
if [ -z "$LOG_PATH" ]; then
    LOG_PATH=/tmp
fi
SERIAL_DEVICE=/dev/serial/by-id/"$(ls /dev/serial/by-id/)"
BAUD_RATE=115200

LOG_FILE=${LOG_PATH}/minicom_"$(date '+%Y-%m-%d_%H-%M')".log
minicom -C "${LOG_FILE}" -con -D "${SERIAL_DEVICE}" -b "${BAUD_RATE}" -w
